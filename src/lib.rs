#[macro_use]
extern crate failure;

use std::ffi::CString;
use std::io::{self, Read, Write};
use std::os::raw;
use std::os::unix::ffi::OsStrExt;
use std::os::unix::io::RawFd;
use std::path::Path;
use std::ptr;

pub mod sys;

#[derive(Fail, Debug)]
#[fail(display = "Could not allocate memory for channel")]
pub struct MemoryError;

#[derive(Fail, Debug)]
#[fail(display = "Generic Xen error")]
pub struct XenError;

#[derive(Debug)]
pub enum ChannelType {
    Server { read_min: usize, write_min: usize },
    Client,
}

#[derive(Debug)]
pub enum OpenStatus {
    Open,
    Closed,
    NotYetConnected,
}

#[derive(Debug)]
pub struct Channel {
    chan: *mut sys::libxenvchan,
}

impl Channel {
    pub fn new<P>(which: ChannelType,
                  domain: raw::c_int,
                  xs_path: P)
                  -> Result<Channel, MemoryError>
        where P: AsRef<Path>
    {
        use ChannelType::*;

        let xs_path = unsafe {
            let p = xs_path.as_ref().as_os_str().as_bytes();
            CString::from_vec_unchecked(p.to_vec())
        };

        let chan = match which {
            Server {
                read_min,
                write_min,
            } => unsafe {
                sys::libxenvchan_server_init(ptr::null_mut(),
                                             domain,
                                             xs_path.as_ptr(),
                                             read_min,
                                             write_min)
            },
            Client => unsafe {
                sys::libxenvchan_client_init(ptr::null_mut(), domain, xs_path.as_ptr())
            },
        };

        if chan.is_null() {
            Err(MemoryError)
        } else {
            Ok(Channel { chan })
        }
    }

    pub fn buffer_space_available(&mut self) -> usize {
        let avail = unsafe { sys::libxenvchan_buffer_space(self.chan) };
        if avail < 0 {
            unreachable!()
        } else {
            avail as usize
        }
    }

    pub fn data_ready(&mut self) -> usize {
        let ready = unsafe { sys::libxenvchan_data_ready(self.chan) };
        if ready < 0 {
            unreachable!()
        } else {
            ready as usize
        }
    }

    pub fn wait(&mut self) -> Result<(), XenError> {
        let ret = unsafe { sys::libxenvchan_wait(self.chan) };
        if ret == 0 { Ok(()) } else { Err(XenError) }
    }

    pub fn status(&mut self) -> OpenStatus {
        let ret = unsafe { sys::libxenvchan_is_open(self.chan) };
        match ret {
            1 => OpenStatus::Open,
            2 => OpenStatus::NotYetConnected,
            _ => OpenStatus::Closed,
        }
    }

    pub fn fd_for_select(&mut self) -> RawFd {
        unsafe { sys::libxenvchan_fd_for_select(self.chan) }
    }
}

impl Drop for Channel {
    fn drop(&mut self) {
        unsafe { sys::libxenvchan_close(self.chan) };
    }
}

impl Read for Channel {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let ret = unsafe {
            sys::libxenvchan_read(self.chan, buf.as_mut_ptr() as *mut raw::c_void, buf.len())
        };
        if ret == -1 {
            Err(io::Error::new(io::ErrorKind::Other, "failed to read from channel"))
        } else {
            Ok(ret as usize)
        }
    }

    fn read_exact(&mut self, buf: &mut [u8]) -> io::Result<()> {
        let ret = unsafe {
            sys::libxenvchan_recv(self.chan, buf.as_mut_ptr() as *mut raw::c_void, buf.len())
        };
        match ret {
            -1 => Err(io::Error::new(io::ErrorKind::Other, "failed to read from channel")),
            0 => Err(io::Error::new(io::ErrorKind::WouldBlock, "not enough data")),
            _ => Ok(()),
        }
    }
}

impl Write for Channel {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let ret = unsafe {
            sys::libxenvchan_write(self.chan, buf.as_ptr() as *const raw::c_void, buf.len())
        };
        if ret == -1 {
            Err(io::Error::new(io::ErrorKind::Other, "failed to write to channel"))
        } else {
            Ok(ret as usize)
        }
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }

    fn write_all(&mut self, buf: &[u8]) -> io::Result<()> {
        let ret = unsafe {
            sys::libxenvchan_send(self.chan, buf.as_ptr() as *const raw::c_void, buf.len())
        };
        match ret {
            -1 => Err(io::Error::new(io::ErrorKind::Other, "failed to write to channel")),
            0 => Err(io::Error::new(io::ErrorKind::WouldBlock, "not enough space")),
            _ => Ok(()),
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
